import VueRouter from 'vue-router';
import LoginComponent from './components/LoginComponent.vue';
import Vue from 'vue';
import ModelListComponent from '@/components/ModelListComponent.vue';

Vue.use(VueRouter);

export default new VueRouter(
  {
    routes: [
      {path: '/login', component: LoginComponent},
      {path: '/contracts/:contractId/models', component: ModelListComponent},
    ],
  }
);
