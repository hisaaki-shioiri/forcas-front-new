import Vue from 'vue';
import VeeValidator, {Validator} from 'vee-validate';
import ja from 'vee-validate/dist/locale/ja';

Vue.use(VeeValidator)
Validator.localize('ja', ja)

