import axios from './api';
import { AxiosResponse } from 'axios';
import Auth from '@/model/Auth';

export function login(auth: Auth): Promise<AxiosResponse> {
  return axios.post('login', {
    mailAddress: auth.email,
    password: auth.password
  });
}
