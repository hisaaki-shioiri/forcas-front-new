import { AxiosResponse } from 'axios';
import axios from './api';

export function simplifiedRoot(): Promise<AxiosResponse<SimplifiedRoot>> {
  return axios.get('simplifiedRoot');
}

export interface SimplifiedRoot {
  groups: Group[]

}

export interface Group {
  groupId: number,
  products: Product[]
}

export interface Product {
  productId: number,
  name: string,
  status: string
}
