import { createLocalVue, shallowMount, Wrapper } from '@vue/test-utils';
import LoginComponent from '@/components/LoginComponent.vue';
import { login } from '@/client/AuthClient';
import flushPromises from 'flush-promises';
import VeeValidate, { Validator } from 'vee-validate';
import ja from 'vee-validate/dist/locale/ja';
import merge from 'lodash.merge';
import { when } from 'jest-when';
import Auth from '@/model/Auth';


jest.mock('../../../src/client/AuthClient.ts');

const localVue = createLocalVue();
localVue.use(VeeValidate);
Validator.localize('ja', ja);

describe('LoginComponent.vue', () => {

  function createWrapper(overrides: any = {}) {
    const options = {
      localVue,
      sync: false,
    };
    return shallowMount(LoginComponent, merge(options, overrides));
  }


  it('ログイン画面が表示できること', () => {
    const wrapper = createWrapper();
    expect(wrapper.find('h1').text()).toContain('ログイン');
    expect(wrapper.contains('#login-error')).toEqual(false);
  });

  it('認証が成功した場合モデル一覧に遷移できること', async () => {
    expect.assertions(1);
    when(login as jest.Mock)
      .calledWith(
        expect.any(Auth)
      )
      .mockResolvedValue({
        data: {
          relatedGroupIds: [100],
        },
      });

    const push = jest.fn();

    const wrapper = createWrapper({
      mocks: {
        $router: {
          push
        }
      },
    });

    wrapper.find('#email').setValue('siosio@uzabase.com');
    wrapper.find('#password').setValue('test2');
    wrapper.find('#login').trigger('click');
    await flushPromises();

    expect(push).toHaveBeenCalledWith('/contracts/100/models')
  });

  it('メールアドレスが未入力の場合はバリデーションエラーになること ', async () => {
    const wrapper = createWrapper();
    wrapper.find('#password').setValue('test2');
    wrapper.find('#login').trigger('click');
    await flushPromises();

    expect(wrapper.find('#email + span').text()).toEqual('emailは必須項目です');
  });

  it('パスワードが未入力の場合はバリデーションエラーになること', async () => {
    const wrapper = createWrapper();
    wrapper.find('#email').setValue('siosio@uzabase.com');
    wrapper.find('#login').trigger('click');
    await flushPromises();

    expect(wrapper.find('#password + span').text()).toEqual('passwordは必須項目です');
  });

  it('認証失敗の場合は、ログインに失敗したよエラーが表示されること', async () => {
    const wrapper = createWrapper();

    when(login as jest.Mock)
      .calledWith(expect.any(Auth))
      .mockRejectedValue({})

    wrapper.find('#email').setValue('siosio@uzabase.com');
    wrapper.find('#password').setValue('test2');
    wrapper.find('#login').trigger('click');
    await flushPromises();

    expect(wrapper.find('#login-error').text()).toEqual('ログインに失敗したよ');
  });

});
